#!/usr/bin/env python3
"""Original code and data from https://www.superdatascience.com/pages/machine-learning"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVR

if __name__ == '__main__':
    df = pd.read_csv('../data/experience_salary.csv')
    X = df.iloc[:, 0:1].values
    # Note syntax to avoid "Reshape your data either using array.reshape(-1,
    # 1) if your data has a single feature or array.reshape(1, -1) if it
    # contains a single sample." error
    y = df.iloc[:, 1:2].values

    # Scale
    sc_X = StandardScaler()
    sc_y = StandardScaler()
    X = sc_X.fit_transform(X)
    y = sc_y.fit_transform(y)

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.20, random_state=0)

    # Fit regressor
    regressor = SVR(kernel='rbf', gamma='auto')
    # squeeze to avoid "DataConversionWarning: A column-vector y was passed when a 1d array was expected.
    # Please change the shape of y to (n_samples, ), for example using
    # ravel(). y = column_or_1d(y, warn=True)"
    regressor.fit(X_train, np.squeeze(y_train, axis=1))

    # Visualise results
    # Note that scaled X and y is inversed back to original scale
    plt.scatter(sc_X.inverse_transform(X_test),
                sc_y.inverse_transform(y_test), color='red')
    plt.scatter(
        sc_X.inverse_transform(X_test),
        sc_y.inverse_transform(
            regressor.predict(X_test).reshape(-1, 1)),
        color='blue')
    plt.title('SVR real vs predicted data')
    plt.xlabel('Experience')
    plt.ylabel('Salary')
    plt.show()

print('Done')
