#!/usr/bin/env python3
"""Original code and data from https://www.superdatascience.com/pages/machine-learning"""
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeRegressor

if __name__ == '__main__':
    df = pd.read_csv('../data/experience_salary.csv')
    X = df.iloc[:, 0:1].values
    y = df.iloc[:, 1].values

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.20, random_state=0)

    regressor = DecisionTreeRegressor(random_state=0, max_depth=None,
                                      min_samples_split=2, min_samples_leaf=1)
    regressor.fit(X_train, y_train)

    # Visualise results
    plt.scatter(X_test, y_test, color='red')
    plt.scatter(X_test, regressor.predict(X_test), color='blue')
    plt.title('Decision tree real vs predicted data')
    plt.xlabel('Experience')
    plt.ylabel('Salary')
    plt.show()

    print('Done')
