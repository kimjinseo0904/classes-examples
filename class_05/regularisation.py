#!/usr/bin/env python3
"""Original code from https://medium.com/coinmonks/regularization-of-linear-models-with-sklearn-f88633a93a2"""

import math

from sklearn.datasets import load_boston
from sklearn.linear_model import ElasticNet
from sklearn.linear_model import Lasso
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import StandardScaler


# Pylint error W0621: Redefining name 'X' from outer scope (line 61) (redefined-outer-name)
# pylint: disable=redefined-outer-name
def print_scores(X_train, X_test, y_train, y_test, regressor, label):
    print('{0} training score: {1:.2f}'.format(
        label, regressor.score(X_train, y_train)))
    print('{0} test score: {1:.2f}'.format(
        label, regressor.score(X_test, y_test)))

    y_pred = regressor.predict(X_test)
    mse = mean_squared_error(y_test, y_pred)
    rmse = math.sqrt(mse)

    print('{0} RMSE: {1:.2f}'.format(label, rmse))


if __name__ == '__main__':
    # https://scikit-learn.org/stable/datasets/index.html#boston-house-prices-dataset
    X, y = load_boston(return_X_y=True)

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    lr_model = LinearRegression()
    lr_model.fit(X_train, y_train)
    print_scores(X_train, X_test, y_train, y_test, lr_model, 'Not regularised')

    steps = [
        ('scalar', StandardScaler()),
        ('poly', PolynomialFeatures(degree=2)),
        ('model', LinearRegression())
    ]
    pipeline = Pipeline(steps)
    pipeline.fit(X_train, y_train)
    print_scores(X_train, X_test, y_train, y_test, pipeline, 'Standardised')

    steps = [
        ('scalar', StandardScaler()),
        ('poly', PolynomialFeatures(degree=2)),
        ('model', Ridge(alpha=10, fit_intercept=True))
    ]
    ridge_pipe = Pipeline(steps)
    ridge_pipe.fit(X_train, y_train)
    print_scores(X_train, X_test, y_train, y_test, ridge_pipe, 'Ridge')

    steps = [
        ('scalar', StandardScaler()),
        ('poly', PolynomialFeatures(degree=2)),
        ('model', Lasso(alpha=0.3, fit_intercept=True))
    ]
    lasso_pipe = Pipeline(steps)
    lasso_pipe.fit(X_train, y_train)
    print_scores(X_train, X_test, y_train, y_test, lasso_pipe, 'Lasso')

    steps = [
        ('scalar', StandardScaler()),
        ('poly', PolynomialFeatures(degree=2)),
        ('model', ElasticNet(alpha=0.3, fit_intercept=True))
    ]
    elastic_pipe = Pipeline(steps)
    elastic_pipe.fit(X_train, y_train)
    print_scores(X_train, X_test, y_train, y_test, elastic_pipe, 'Elastic')

    print('Done')
