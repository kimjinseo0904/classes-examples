#!/usr/bin/env python3
"""Original code and data from https://www.superdatascience.com/pages/machine-learning"""
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split

if __name__ == '__main__':
    df = pd.read_csv('../data/experience_salary.csv')
    X = df.iloc[:, 0:1].values
    y = df.iloc[:, 1].values

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.20, random_state=0)

    regressor = RandomForestRegressor(n_estimators=10, random_state=0)
    regressor.fit(X, y)

    # Visualise results
    plt.scatter(X_test, y_test, color='red')
    plt.scatter(X_test, regressor.predict(X_test), color='blue')
    plt.title('Random forest real vs predicted data')
    plt.xlabel('Experience')
    plt.ylabel('Salary')
    plt.show()

    print('Done')
