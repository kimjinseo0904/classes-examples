#!/usr/bin/env python3
"""code from https://machinelearningmastery.com/machine-learning-in-python-step-by-step/"""

# Python version
import sys

import matplotlib
import numpy
import pandas
import scipy
import sklearn

# Check the versions of libraries
print('Python: {}'.format(sys.version))
# scipy
print('scipy: {}'.format(scipy.__version__))
# numpy
print('numpy: {}'.format(numpy.__version__))
# matplotlib
print('matplotlib: {}'.format(matplotlib.__version__))
# pandas
print('pandas: {}'.format(pandas.__version__))
# scikit-learn
print('sklearn: {}'.format(sklearn.__version__))

# Load dataset
names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
# You could also read csv file "from url"
# url = "https://raw.githubusercontent.com/jbrownlee/Datasets/master/iris.csv"
# dataset = pandas.read_csv(url, names=names)
dataset = pandas.read_csv('../data/iris.csv', names=names)

print('Done')
