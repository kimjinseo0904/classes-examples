#!/usr/bin/env python
"""Original code and data from https://www.superdatascience.com/pages/machine-learning"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split

df = pd.read_csv('../data/experience_salary.csv')

X = df.loc[:, 'YearsExperience'].values
y = df.loc[:, 'Salary'].values

# Note that .loc returns 1D array, but linear regressor requires at least 2D
X = np.expand_dims(X, axis=1)

# Split train test sets
X_train, X_test, y_train, y_test = train_test_split(
    X, y, test_size=0.25, random_state=0)

# Fit Simple Linear Regression to the training set
regressor = LinearRegression()
regressor.fit(X_train, y_train)

# Predict the test set results
y_pred = regressor.predict(X_test)

# Visualise the training set results
plt.figure('fig. 1')
plt.scatter(X_train, y_train, color='red', marker='x', s=7)
plt.plot(X_train, regressor.predict(X_train), color='blue')
plt.title('Salary vs Experience (Training set)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')

# Visualise the test set results
plt.figure('fig. 2')
plt.scatter(X_test, y_test, color='red', label='Test data')
plt.scatter(X_test, y_pred, color='green', label='Predicted data')
plt.title('Salary vs Experience (Test set)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')
plt.legend(loc=2)

plt.show()

print('Done')
