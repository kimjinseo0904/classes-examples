#!/usr/bin/env python3
import numpy as np
import pandas as pd
from sklearn.compose import make_column_transformer
from sklearn.impute import SimpleImputer
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import OrdinalEncoder

if __name__ == '__main__':
    # Modified data from
    # https://www.superdatascience.com/pages/machine-learning
    df = pd.read_csv('../data/purchased_products.csv')

    # Extract features
    X = df[['Has previous version', 'Size', 'Country', 'Age', 'Salary']].values
    # Extract dependent variable
    y = df['Purchased'].values

    # Replace missing numeric values with mean
    imp = SimpleImputer(missing_values=np.nan, strategy='mean')
    imp = imp.fit(X[:, [3, 4]])
    X[:, [3, 4]] = imp.transform(X[:, [3, 4]])

    # Categorise nominal categorical features
    ct = make_column_transformer(
        (OneHotEncoder(), [0, 2]), remainder="passthrough", sparse_threshold=0)
    X = ct.fit_transform(X)
    # Note that columns order was changed after transform and dummy variables
    # are not dropped

    # Categorise ordinal features
    # size_col = X[:, 5]
    size_col = np.expand_dims(X[:, 5], axis=1)
    ord_enc = OrdinalEncoder(categories=[['XS', 'S', 'M', 'L', 'XL'], ])
    X[:, 5] = np.squeeze(ord_enc.fit_transform(size_col), axis=1)

    # Categorise dependent variable
    ct = make_column_transformer((OneHotEncoder(), [0]), sparse_threshold=0)
    # Note syntax to avoid "IndexError: tuple index out of range"
    y = np.expand_dims(y, axis=1)
    y = ct.fit_transform(y)

    # Split training and test sets
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    print('Done')
