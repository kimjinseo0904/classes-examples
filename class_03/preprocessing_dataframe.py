#!/usr/bin/env python3
import pandas as pd

if __name__ == '__main__':
    # Modified data from
    # https://www.superdatascience.com/pages/machine-learning
    df = pd.read_csv('../data/purchased_products.csv')

    # Replace missing numeric values with mean
    df['Age'] = df['Age'].fillna(value=df['Age'].mean())
    df['Salary'] = df['Salary'].fillna(value=df['Salary'].mean())

    # Encode size ordinal naive way
    size_dict = {'XS': 0, 'S': 1, 'M': 2, 'L': 3, 'XL': 4, 'XXL': 5}
    df['Size'] = df.Size.map(size_dict)

    # Encode nominal categorical data
    for column in ('Has previous version', 'Country', 'Purchased'):
        # Pylint error R0204: Redefinition of df type from pandas.io.parsers.readers.TextFileReader
        # to pandas.core.frame.DataFrame (redefined-variable-type)
        # pylint: disable=redefined-variable-type
        df = pd.get_dummies(df, prefix=[column], columns=[column])

    # Reorder columns. Perhaps manual is easiest for small amount of columns
    # See https://github.com/pandas-dev/pandas/issues/17612 for alternative
    # solutions
    df = df[['Has previous version_No',
             'Has previous version_Yes',
             'Size',
             'Country_France',
             'Country_Germany',
             'Country_Spain',
             'Age',
             'Salary',
             'Purchased_No',
             'Purchased_Yes']]

    print('Done!')
