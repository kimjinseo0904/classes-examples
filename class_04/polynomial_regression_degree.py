#!/usr/bin/env python3
"""Code from https://acadgild.com/blog/polynomial-regression-understand-power-of-polynomials"""
import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import load_boston
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures

if __name__ == '__main__':
    # https://scikit-learn.org/stable/datasets/index.html#boston-house-prices-dataset
    X, y = load_boston(return_X_y=True)

    X = X[:, 12]  # Use only LSTAT
    X = np.expand_dims(X, axis=1)

    x_train, x_test, y_train, y_test = train_test_split(X, y, test_size=0.10,
                                                        random_state=42,
                                                        shuffle=True)
    # Polynomial Regression-nth order
    plt.scatter(x_test, y_test, s=10, alpha=0.3)

    for degree in (1, 2, 3, 4, 5, 6, 7):
        model = make_pipeline(PolynomialFeatures(degree), LinearRegression())
        model.fit(x_train, y_train)
        plt.plot(
            x_test,
            model.predict(x_test),
            label="degree %d" %
            degree +
            '; $R^2$: %.2f' %
            model.score(
                x_test,
                y_test))

    plt.legend(loc='upper right')
    plt.xlabel("Test LSTAT Data")
    plt.ylabel("Predicted Price")
    plt.title("Variance Explained with Varying Polynomial")
    plt.show()

    print('Done')
