#!/usr/bin/env python3
"""Original code and data from https://www.superdatascience.com/pages/machine-learning"""

import numpy as np
import pandas as pd
import statsmodels.api as sm
from sklearn.compose import make_column_transformer
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder


# Pylint error W0621: Redefining name 'X' from outer scope (line 61) (redefined-outer-name)
# pylint: disable=redefined-outer-name
def backward_elimination(X, y, p_threshold=0.05):
    # Create fake intercept by adding column with ones
    X = np.append(arr=np.ones((np.shape(X)[0], 1)).astype(
        int), values=X, axis=1)

    while True:
        # Fit the full model with all possible predictors
        regressor_ols = sm.OLS(endog=y, exog=X).fit()
        # print(regressor_ols.summary())
        remove_i = np.argmax(regressor_ols.pvalues)
        if regressor_ols.pvalues[remove_i] < p_threshold:
            return X
        # print('Remove column:', remove_i + 1)
        X = np.delete(X, remove_i, axis=1)


def forward_regression(X, y, threshold_in, verbose=False):
    """Function from https://github.com/AakkashVijayakumar/stepwise-regression/blob/master/stepwise_regression/step_reg.py"""
    initial_list = []
    included = list(initial_list)
    while True:
        changed = False
        excluded = list(set(X.columns) - set(included))
        new_pval = pd.Series(index=excluded, dtype='float64')
        for new_column in excluded:
            model = sm.OLS(y, sm.add_constant(
                pd.DataFrame(X[included + [new_column]]))).fit()
            new_pval[new_column] = model.pvalues[new_column]
        best_pval = new_pval.min()
        # Pylint error R6103: Use 'if (best_pval := new_pval.min()) < threshold_in:'
        # instead (consider-using-assignment-expr)
        # pylint: disable=consider-using-assignment-expr
        if best_pval < threshold_in:
            best_feature = new_pval.idxmin()
            included.append(best_feature)
            changed = True
            if verbose:
                print(
                    'Add  {:30} with p-value {:.6}'.format(best_feature, best_pval))

        if not changed:
            break

    return included


if __name__ == '__main__':
    # Modified data from
    # https://www.superdatascience.com/pages/machine-learning
    df = pd.read_csv('../data/startups.csv')

    X = df.iloc[:, :-1].values
    y = df.iloc[:, 4].values

    # Encode State
    ct = make_column_transformer(
        (OneHotEncoder(), [3]), remainder="passthrough", sparse_threshold=0)
    X = ct.fit_transform(X)
    # Change type to be float for all
    X = X.astype(float)
    # Remove first column to avoid dummy variable trap
    X = np.delete(X, 0, axis=1)

    # Predict test values for "all-in" features
    X_train_all_f, X_test_all_f, y_train_all_f, y_test_all_f = train_test_split(
        X, y, test_size=0.2, random_state=0)
    regressor_all_f = LinearRegression()
    regressor_all_f.fit(X_train_all_f, y_train_all_f)
    y_pred_all_f = regressor_all_f.predict(X_test_all_f)

    # Predict after backward elimination
    X_back = backward_elimination(X, y)
    X_train_back, X_test_back, y_train_back, y_test_back = train_test_split(
        X_back, y, test_size=0.2, random_state=0)
    regressor_back = LinearRegression()
    regressor_back.fit(X_train_back, y_train_back)
    y_pred_back = regressor_back.predict(X_test_back)

    # Predict after forward selection
    # Current forward selection works with pandas data frame. Create one from
    # features
    df_fwd = pd.DataFrame(X)
    included = forward_regression(df_fwd, y, 0.05)
    # Included columns list is returned. Select features based on that list
    X_fwd = X[:, included]
    X_train_fwd, X_test_fwd, y_train_fwd, y_test_fwd = train_test_split(
        X_fwd, y, test_size=0.2, random_state=0)
    regressor_back = LinearRegression()
    regressor_back.fit(X_train_fwd, y_train_fwd)
    y_pred_fwd = regressor_back.predict(X_test_fwd)

    print('Done')
