#!/usr/bin/env python3
"""Original code and data from https://www.superdatascience.com/pages/machine-learning"""
import pandas as pd
from common import plot_classification, print_confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC

if __name__ == '__main__':
    title = 'SVM'
    df = pd.read_csv('../data/social_network_ads.csv')
    # To avoid "DataConversionWarning: Data with input dtype int64 was
    # converted to float64 by StandardScaler.
    X = df.iloc[:, [2, 3]].values.astype(float)
    y = df.iloc[:, 4].values.astype(float)

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.25, random_state=0)

    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    clf = SVC(kernel='rbf', random_state=0)
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)

    plot_classification(X_train, y_train, clf, title + ' (Training set)')
    plot_classification(X_test, y_test, clf, title + ' (Test set)')

    print_confusion_matrix(y_test, y_pred, label=title)
    cm = confusion_matrix(y_test, y_pred)

    print(title + ' accuracy:', accuracy_score(y_test, y_pred))

    print('Done')
