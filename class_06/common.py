import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.colors import ListedColormap
from sklearn.metrics import confusion_matrix


def plot_classification(X_set, y_set, clf, title):
    X1, X2 = np.meshgrid(
        np.arange(start=X_set[:, 0].min() - 1,
                  stop=X_set[:, 0].max() + 1, step=0.01),
        np.arange(start=X_set[:, 1].min() - 1, stop=X_set[:, 1].max() + 1,
                  step=0.01))
    plt.contourf(X1, X2, clf.predict(np.array([X1.ravel(),
                                               X2.ravel()]).T).reshape(X1.shape),
                 alpha=0.75, cmap=ListedColormap(('red', 'green')))
    plt.xlim(X1.min(), X1.max())
    plt.ylim(X2.min(), X2.max())

    for i, j in enumerate(np.unique(y_set)):
        plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1],
                    # array is used to avoid "'c' argument looks like a single
                    # numeric RGB or RGBA sequence" warning
                    c=np.array([ListedColormap(('black', 'white'))(i)]),
                    label=j)

    plt.title(title)
    plt.xlabel('Age')
    plt.ylabel('Estimated Salary')
    plt.legend()
    plt.show()


def print_confusion_matrix(y_true, y_pred, label=''):
    # Thanks stackoverflow https://stackoverflow.com/a/50326049
    cmtx = pd.DataFrame(confusion_matrix(y_true, y_pred),
                        index=['true:0', 'true:1'],
                        columns=['pred:0', 'pred:1'])
    print('# %s%sonfusion matrix' % (label, ' c'if label else 'C'))
    print(cmtx, '\n')
