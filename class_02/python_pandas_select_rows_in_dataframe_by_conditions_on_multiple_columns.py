#!/usr/bin/env python3
"""code from https://thispointer.com/python-pandas-select-rows-in-dataframe-by-conditions-on-multiple-columns/"""
import pandas as pd


def main():
    # List of Tuples
    students = [('jack', 'Apples', 34),
                ('Riti', 'Mangos', 31),
                ('Aadi', 'Grapes', 30),
                ('Sonia', 'Apples', 32),
                ('Lucy', 'Mangos', 33),
                ('Mike', 'Apples', 35)
                ]
    # Create a DataFrame object
    dfObj = pd.DataFrame(students, columns=['Name', 'Product', 'Sale'])
    print("Original Dataframe", dfObj, sep='\n')

    # Select Rows based on value in a column
    subsetDataFrame = dfObj[dfObj['Product'] == 'Apples']
    print("DataFrame with Product : Apples", subsetDataFrame, sep='\n')
    filteringSeries = dfObj['Product'] == 'Apples'
    print("Filtering Series", filteringSeries, sep='\n')
    subsetDataFrame = dfObj[filteringSeries]
    print("DataFrame with Product : Apples", subsetDataFrame, sep='\n')

    # Select Rows based on any of the multiple values in a column
    subsetDataFrame = dfObj[dfObj['Product'].isin(['Mangos', 'Grapes'])]
    print("DataFrame with Product : Mangos or Grapes", subsetDataFrame, sep='\n')

    # Select DataFrame Rows Based on multiple conditions on columns
    filterinfDataframe = dfObj[(dfObj['Sale'] > 30) & (dfObj['Sale'] < 33)]
    print("DataFrame with Sales between 31 to 32", filterinfDataframe, sep='\n')


if __name__ == '__main__':
    main()
