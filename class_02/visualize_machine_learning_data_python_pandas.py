#!/usr/bin/env python3
"""code from https://machinelearningmastery.com/visualize-machine-learning-data-python-pandas/"""
import matplotlib.pyplot as plt
import numpy
import pandas
from pandas.plotting import scatter_matrix

names = ['preg', 'plas', 'pres', 'skin',
         'test', 'mass', 'pedi', 'age', 'class']
data = pandas.read_csv('../data/pima-indians-diabetes-data.csv', names=names)

print('Numeric values statistics:')
# Note that float format is set
pandas.set_option('float_format', '{:f}'.format)
# To not limit output columns based on terminal with
pandas.options.display.width = 0
print(data.describe(include='all'))
print('\n')

# Univariate Histograms
data.hist()
plt.show()

# Univariate Density Plots
data.plot(kind='density', subplots=True, layout=(3, 3), sharex=False)
plt.show()

# Box and Whisker Plots
data.plot(kind='box', subplots=True, layout=(3, 3), sharex=False, sharey=False)
plt.show()

# Correction Matrix Plot
correlations = data.corr()
# plot correlation matrix
fig = plt.figure()
ax = fig.add_subplot(111)
cax = ax.matshow(correlations, vmin=-1, vmax=1)
fig.colorbar(cax)
ticks = numpy.arange(0, 9, 1)
ax.set_xticks(ticks)
ax.set_yticks(ticks)
ax.set_xticklabels(names)
ax.set_yticklabels(names)
plt.show()

# Scatterplot Matrix
scatter_matrix(data)
plt.show()
