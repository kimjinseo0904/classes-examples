#!/usr/bin/env python3
"""code from https://machinelearningmastery.com/machine-learning-in-python-step-by-step/"""


import matplotlib.pyplot as plt
import pandas
from pandas.plotting import scatter_matrix

# Load dataset
names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
# You could also read csv file "from url"
# url = "https://raw.githubusercontent.com/jbrownlee/Datasets/master/iris.csv"
# dataset = pandas.read_csv(url, names=names)
dataset = pandas.read_csv('../data/iris.csv', names=names)

# shape
print(dataset.shape)

# head
print(dataset.head(20))

# descriptions
print(dataset.describe())

# class distribution
print(dataset.groupby('class').size())

# box and whisker plots
dataset.plot(kind='box', subplots=True, layout=(
    2, 2), sharex=False, sharey=False)
plt.show()

# histograms
dataset.hist()
plt.show()

# scatter plot matrix
scatter_matrix(dataset)
plt.show()
